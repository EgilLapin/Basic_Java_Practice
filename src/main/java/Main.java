import Vehicles.Moto.ZX666R.KawasakiZX666R;

public class Main {

    /**
     * No unit tests, console prints will be enough for now...
     *
     * @author  NINJA ZX666R
     * @version 1.3
     * @Date 24.11.2022
     */

    public static void main(String[] args) {
//        System.out.println("/////////////////Car////////////////////");
//        Vehicle myHybrid = new Car(6.66,1);
//        myHybrid.getFuelLevel(); //Print fuel remaining
//        myHybrid.addFuel(46.66); // Tank 46.66 litres
//        myHybrid.drive(666); // Drive 666 km
//        myHybrid.getFuelLevel(); //Print fuel remaining
//
//        //add too much fuel
//        myHybrid.addFuel(63.362266666666673); // Tank extra 63.362266666666673 litres
//        myHybrid.getFuelLevel(); //Print fuel remaining
//        myHybrid.drive(200.300300300300300); // Try Drive 200 km
//        myHybrid.getFuelLevel(); //Print fuel remaining
//
//        //Checking for negative values :)
//        myHybrid.drive(-666); // Drive -666 km
//        myHybrid.addFuel(-13); // Tank -13 litres
//        myHybrid.getFuelLevel(); //Print fuel remaining
//
//        System.out.println("/////////////////MOTO////////////////////");
//        Motorcycle myMotorcycle = new Motorcycle(7.77,3);
//        myMotorcycle.getFuelLevel(); //Print fuel
//        myMotorcycle.addFuel(11.11); // Tank 11.11 litres
//        myMotorcycle.drive(200); // Drive 200 km
//        myMotorcycle.getFuelLevel(); //Print fuel remaining
//
//        myMotorcycle.addFuel(222.222); // Tank extra 222.222 litres
//        myMotorcycle.getFuelLevel(); //Print fuel remaining
//        myMotorcycle.drive(100); // Try Drive 100 km
//        myMotorcycle.getFuelLevel(); //Print fuel remaining
//
//        //Checking for negative values :)
//        myMotorcycle.drive(-666); // Drive -666 km
//        myMotorcycle.addFuel(-13); // Tank -13 litres
//        myMotorcycle.getFuelLevel(); //Print fuel remaining
//
//        myMotorcycle.drive(80); // Try Drive 80 km
//        myMotorcycle.getFuelLevel(); //Print fuel remaining*/
//        System.out.println("/////////////////KAWA/////////////////////");
        KawasakiZX666R zx6r = new KawasakiZX666R(15,15);
        zx6r.getFuelLevel();
        zx6r.drive(100);
        zx6r.getFuelLevel(); //Print fuel remaining
        zx6r.addFuel(4);
        zx6r.addFuel(1);
        zx6r.getFuelLevel(); //Print fuel remaining
        zx6r.drive(222);
        zx6r.drive(-100);
        zx6r.addFuel(-2.22);
        zx6r.getFuelLevel(); //Print fuel remaining
        zx6r.fullSend200(99.999f);
        System.out.println("Few hours later.....");
        zx6r.maybeAnotherFullSend200Today();
        zx6r.fullSend200(99.999f);
    }
}
